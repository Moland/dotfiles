install_link_file() {
	local TARGET=$1
	local SOURCE=$2
	shift 2

	echo_link_info() {
		local BASE=$(basename $1)
		echo -en "~/${BASE} => ${2}"
	}

	echo_link_info $TARGET $SOURCE

	if [[ -h $TARGET ]]; then
		echo " - Is already a symlink..."
		return
	elif [[ -e $TARGET ]]; then
		echo
		echo -n " - Target already exists; replace? [(Y)es|*(N)o|(P)rint contents|make (B)ackup]: "
		read -rsN1 replace
		echo -en "\e[1K\r\e[1A\e[K\r"

		case "${replace,,}" in
			"p")
				if [[ -d $TARGET ]]; then
					ls -lAh $TARGET | less
				else
					less $TARGET
				fi
				link_file $TARGET $SOURCE
				return
			;;
			"b")
				echo_link_info $TARGET $SOURCE
				echo -en " - Making backup"
				mv $TARGET "${TARGET}.backup"
			;;
			"y")
				echo_link_info $TARGET $SOURCE
				echo -en " - Removing"
				rm $TARGET
			;;
			*)
				echo_link_info $TARGET $SOURCE
				echo " - Skipping"
				return
			;;
		esac
	fi

	echo -en " - Linking"
	link $(readlink -f $SOURCE) $TARGET
	echo " - OK"
}
