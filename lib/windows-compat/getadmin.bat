@echo off

: NOTE
: * Takes one string argument of command to run with elevated privileges.
: * Escaping of command is not bullet proof at all.
: * Admin elevated cmd calls always start in System32, so %CD% is not available
:   once gotAdmin is reached. We therefor have to pass the CD to the UAC cmd.
: * Does not yet work with relative paths...

REM --> Check for permissions.
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> Go to UAC prompt if error, else move on.
if '%errorlevel%' NEQ '0' ( goto UACPrompt ) else ( goto gotAdmin )

REM --> Request administrative privileges.
:UACPrompt
	set vbs="%temp%\getadmin.vbs"
	set command=%~1
	set command=%command:\"="%
	set command=%command:"=""%

	echo Set UAC = CreateObject^("Shell.Application"^) > %vbs%
	echo UAC.ShellExecute "cmd.exe", "/C %~s0 ""%CD%"" ""%command%""", "", "runas", 1 >> %vbs%

	%vbs%
	del %vbs%
	exit /B

REM --> Privileges obtained and can execute command now.
:gotAdmin
	cd /D "%~1"
	cmd /C "%~2"
