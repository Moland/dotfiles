DIR=$DOTFILES_PATH/lib/windows-compat
CMD=$(basename $0)

source $DIR/utils.sh

is_link() {
	if [[ "$#" -eq 0 ]]; then
		echo "Usage: $CMD LINK_NAME"
		echo
		echo "Validates whether given resource LINK_NAME is a valid symlink."
		return 1
	fi

	if is_windows; then
		fsutil reparsepoint query "$1" > /dev/null
	else
		[[ -L "$1" ]]
	fi
}

link() {
	if [[ "$#" -eq 0 ]]; then
		echo "Usage: $CMD TARGET LINK_NAME"
		echo
		echo "Makes a symbolic link at LINK_NAME, pointing to TARGET. Works in Windows,"
		echo "as well as Linux. Is moddeled upon ln's parameters."
		return 1
	fi

	if is_windows; then
		local DIR_FLAG=""
		local TARGET=$(cygpath -w $1)
		local LINK_NAME=$(cygpath -w $2)

		if [[ -d "$1" ]]; then
			DIR_FLAG="/D"
		fi

		$DIR/getadmin.bat "mklink $DIR_FLAG \"$LINK_NAME\" \"$TARGET\"" # > /dev/null
	else
		ln -s "$1" "$2"
	fi
}
