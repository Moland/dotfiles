# Source global dotfiles exports
source "${HOME}/.dotfiles_env"

# Source global config
source "${DOTFILES_PATH}/conf/global.sh"

# All configuration is in separate config files, so let's source them all. Load
# order is inspired by setup in https://github.com/antonioribeiro/zsh, although
# I'm very uncertain to what extent it matters.

# Saving some keystrokes with a sourcing helper
_s() { source "${DOTFILES_PATH}/conf/zsh/${1}.zsh" }

_s "zplug"
_s "colors"
_s "prompt"
_s "autosuggestion"
_s "completion"
# _s "correction"
_s "expansion"
_s "directories"
_s "aliases"
_s "bindkeys"
_s "history"
_s "misc"
_s "theme"

# Another helper for sourcing functions
_f() { _s "functions/${1}" }

_f "zsh_history_stats"

# Clean up global scope
unfunction _s
unfunction _f

# Load aliases
source "${DOTFILES_PATH}/conf/aliases.sh"
