// Always be smilin'
function redirectToAmazonSmile() {
	if (location.hostname !== "smile.amazon.com") {
		location.replace(`https://smile.amazon.com${location.pathname}${location.search}`);
	}
}

redirectToAmazonSmile();
