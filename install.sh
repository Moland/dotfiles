#!/usr/bin/env bash

# Export DOTFILES_PATH if not already defined
if [[ -z $DOTFILES_PATH ]]; then
	# Set to dirname of install script (this file)
	export DOTFILES_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fi

source $DOTFILES_PATH/lib/windows-compat/link.sh
source $DOTFILES_PATH/lib/install_link_file.sh

echo "Installing dotfiles"
echo "==================="
echo "Please confirm the paths are correct:"
echo
echo "\$HOME = ${HOME}"
echo "\$DOTFILES_PATH = ${DOTFILES_PATH}"
echo
echo -n "Is this correct? [Yes|*No]: "
read -rsN1 confirm
echo -en "\e[1K\r"

if [[ "${confirm,,}" != "y" ]]; then
	echo "Aborting..."
	exit 1
fi

echo "Starting installation"
echo

echo "Symlinking"
echo "----------"
echo "Making symbolic links for configuration files in ~, pointing to dotfiles."
echo

# This list can probably be replaced with a glob for all hidden files in directory...
declare -A SYMLINKS=(
	# ["LINK_NAME"]="TARGET"
	[".bashrc"]=".bashrc"
	# [".bash_history"]=".bash_history"
	[".dir_colors"]=".dir_colors"
	[".dotfiles_env"]=".dotfiles_env"
	[".editorconfig"]=".editorconfig"
	[".gitconfig"]=".gitconfig"
	[".git-stats-config.js"]=".git-stats-config.js"
	# [".hyper.css"]=".hyper.css"
	# [".hyper.js"]=".hyper.js"
	[".inputrc"]=".inputrc"
	[".lesshst"]=".lesshst"
	# [".node_repl_history"]=".node_repl_history"
	[".profile"]=".profile"
	# [".ts_node_repl_history"]=".ts_node_repl_history"
	[".tmux.conf"]=".tmux.conf"
	[".vuerc"]=".vuerc"
	[".Xclients"]=".Xclients"
	[".xinitrc"]=".xinitrc"
	[".yarnrc"]=".yarnrc"
	# [".zcompdump"]=".zcompdump"
	[".zprofile"]=".zprofile"
	[".zshrc"]=".zshrc"
	# [".zsh_history"]=".zsh_history"
	["highlight.css"]="highlight.css"
)
for LINK in ${!SYMLINKS[@]}; do
	TARGET=$(readlink -f $DOTFILES_PATH/${SYMLINKS[$LINK]})
	install_link_file "${HOME}/${LINK}" $TARGET
done
