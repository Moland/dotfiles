" Plugins
" =======
call plug#begin('~/.local/share/nvim/site/plugged')

  Plug 'tpope/vim-sensible'   " Sensible basics
  Plug 'tpope/vim-sleuth'     " Tab identification
  Plug 'jiangmiao/auto-pairs' " Auto pair quotes, parans, brackets
  Plug 'alvan/vim-closetag'   " Auto close tags
  Plug 'neomake/neomake'      " Make linting
  Plug 'tpope/vim-surround'   " CRUD surroundings
  Plug 'tpope/vim-commentary' " Easy comment toggling
  Plug 'tpope/vim-repeat'     " Repeats stuff easily
  Plug 'editorconfig/editorconfig-vim' " Keeps code style consistent
  Plug 'muansari96/vimify'    " Control Spotify
  Plug 'vim-airline/vim-airline' " Airline status bar

  " Git/VCS
  " -------
  Plug 'tpope/vim-git'        " Syntax for git-type files
  Plug 'tpope/vim-fugitive'   " Git interfaces
  Plug 'mhinz/vim-signify'    " Shows VCS changes in gutter

  " NERDTree
  " --------
  Plug 'scrooloose/nerdtree'     " The main tree
  Plug 'jistr/vim-nerdtree-tabs' " Makes NERDTree work better with tabs

  " Themes
  " ------
  "Plug 'altercation/vim-colors-solarized'
  Plug 'frankier/neovim-colors-solarized-truecolor-only'

  " Deoplete completion
  " -------------------
  function! DoRemote(arg)
    UpdateRemotePlugins
  endfunction

  Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
  Plug 'carlitux/deoplete-ternjs' " Tern deoplete addition
  " Tern JS editing support
  Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }

" End og plugin list
call plug#end()

" Basics
" ======
set termguicolors " Enables 24-bit colors
set showcmd       " Shows command completions
set showmode      " Shows mode status
set visualbell    " Disable beep sounds
set autoread      " Reload on external changes
set hidden        " Hides buffers over ditching them

filetype plugin indent on " Load plugin and indent file configs

"set gcr=i:ver1
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 1
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Style
" =====
let g:solarized_termcolors=256
syntax enable
set background=dark
colorscheme solarized

" Gutter
" ======
set number
set relativenumber
set numberwidth=3
set cpoptions+=n

" Completion
" ==========
" Use deoplete
let g:deoplete#enable_at_startup = 1
let g:tern_request_timeout = 1
" Use tern_for_vim
let g:tern#command = ["tern"]
let g:tern#arguments = ["--persistent"]
