# Default arguments
# -----------------

# diff - present diffs side by side and output only left column of common lines.
DIFF_DEFAULT_ARGS="--side-by-side --left-column"

# highlighter - set output to truecolor and set tab size to 2 spaces
HIGHLIGHTER_DEFAULT_ARGS="-O truecolor -t 2"

# ls - make sizes human readable, set sorting to natural version sort, add
# slashes to end of directories, add file type indicators, group directories
# first and set time style to a nicer format.
LS_DEFAULT_ARGS="-hvpF --group-directories-first --time-style=+'%d-%m-%Y %H:%M'"

# Sudo
# ----

# Since only first command is checked for alias, this allows us to use sudo
# without getting lots of "command not found".
alias sudo="sudo "

# Shortcuts
# ---------

# Navigate to dotfiles or open in IDE
alias DOT="cd ${DOTFILES_PATH}"
alias dotfiles="cd ${DOTFILES_PATH}; ${IDE} ${DOTFILES_PATH}"

# Go to projects
alias PROJECTS="cd ${PROJECTS_PATH}"
alias P="projects"

# Apps
# ----

# Neovim
alias vim="nvim"

# Git shorthand and typos
alias g="git"
alias gut="git"
alias got="git"

# Yarn
alias y="yarn"

# Google Chrome
alias google-chrome="google-chrome-stable"
alias chrome="google-chrome"

# Colors
# ------
COLOR_ARG=""

# Set color argument to auto supported.
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors \
		&& eval "$(dircolors -b ~/.dircolors)" \
		|| eval "$(dircolors -b)"

	COLOR_ARG="--color=auto"
fi

# Sane defaults
# -------------

# Human-readable sizes
alias du="du -h"
alias dud="du -d 1"
alias df="df -h"
alias free="free -hltw"

# Force interactive mode with rm, cp and mv, to avoid bad days
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"

# Better diff
alias _diff="command diff ${DIFF_DEFAULT_ARGS} ${COLOR_ARG}"
diff() { _diff --width=$COLUMNS $@; }

# Better ls
alias ls="command ls ${LS_DEFAULT_ARGS} ${COLOR_ARG}"
alias ll="command ls -l ${LS_DEFAULT_ARGS} ${COLOR_ARG}"
alias la="command ls -lAh ${LS_DEFAULT_ARGS} ${COLOR_ARG}"

# Better grep
alias grep="grep ${COLOR_ARG}"
alias fgrep="fgrep ${COLOR_ARG}"
alias egrep="egrep ${COLOR_ARG}"

# Limit default htop to processes owned by current user, and alias for showing
# in tree mode and for all users.
alias htopU="htop -u $USER"
alias htopT="htop -t"
alias htop="htopU"
# alias htopA="command htop"
# alias htopAT="htopA -t"

# Utility
# -------

# less and grep shorthands for cat
catL() { cat $1 | $PAGER; }
catG() { cat $1 | cgrep "${@:2}"; }
copycat() { cat $1 | clipcopy; }
alias catcopy="copycat"

# less and grep shorthands for ls
lsL() { cls $1 | $PAGER; }
lsG() { local re=$1p=$2; shift 2 2> /dev/null; cls $p | cgrep $re ${@:2}; }
lsC() { cls $1 | clipcopy; }

# less and grep shorthands for la
laL() { cla $1 | $PAGER; }
laG() { local re=$1 p=$2; shift 2 2> /dev/null; cla $p | cgrep $re ${@:2}; }
laC() { cla $1 | clipcopy; }

# Better diff
alias vdiff="code --diff"
alias dif="vdiff"
diffL() { cdiff $@ | $PAGER; }

# Lower case aliase for avoiding laL typos
alias catl="catL"
alias catg="catG"
alias lal="laL"
alias lag="laG"
alias diffl="diffL"

# Always continue any previously aborted wget downloads
# alias wget="wget -c"

# Alias for outputting the contents of a file with syntax highlighting
alias highlighter="${HIGHLIGHTER} ${HIGHLIGHTER_DEFAULT_ARGS}"
alias colorize="highlighter"
alias c="colorize"

# Navigation
# ----------

# Move up
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# Move back
alias bk="cd ${OLDPWD}"

# Cleanup
# -------

unset COLOR_ARG
unset DIFF_DEFAULT_ARGS
unset HIGHLIGHTER_DEFAULT_ARGS
unset LS_DEFAULT_ARGS
