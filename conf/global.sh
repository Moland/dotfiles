# Set ulimit to a big number
ulimit -n 2048

# Source exports first, as they may be used pretty much anywhere from now
source "${DOTFILES_PATH}/conf/exports.sh"

# Then source functions
source "${DOTFILES_PATH}/conf/functions.sh"

# Apply lesspipe for friendlier non-text input files with less
# [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Add VTE profile to open new terminals in same path as current
source /etc/profile.d/vte.sh

# Run startup scripts
# source "${DOTFILES_PATH}/scripts/startup/iris.sh"
