# TODO: Refactor into multiple files
# TODO: Check out collections of functions for inspiration:
#       * https://github.com/robbyrussell/oh-my-zsh/blob/master/lib

# Reload shell configurations (zsh or bash)
function reload() {
	if [ -n "$BASH_VERSION" ]; then source $HOME/.bashrc; fi
	if [ -n "$ZSH_VERSION" ]; then source $HOME/.zshrc; fi
}

# TODO: Make clipboard functions cross-platform friendly:
# https://github.com/robbyrussell/oh-my-zsh/blob/master/lib/clipboard.zsh

# Copys current selection to clipboard
# clipcopy() { xclip -in -selection clipboard < "${1:-/dev/stdin}"; }

# Pastes contents of clipboard into selection
# clippaste() { xclip -out -selection clipboard; }

# Weather
weather() { curl "https://wttr.in/${1-}"; }

# Extracts a compressed file
extract() {
	if [ -f $1 ] ; then
		case $1 in
			*.tar.bz2)   tar xjf $1    ;;
			*.tar.gz)    tar xzf $1    ;;
			*.bz2)       bunzip2 $1    ;;
			*.rar)       unrar x $1    ;;
			*.gz)        gunzip $1     ;;
			*.tar)       tar xf $1     ;;
			*.tbz2)      tar xjf $1    ;;
			*.tgz)       tar xzf $1    ;;
			*.zip)       unzip $1      ;;
			*.Z)         uncompress $1 ;;
			*.7z)        7z x $1       ;;
			*)           echo "'$1' cannot be extracted via extract()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}
