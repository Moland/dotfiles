# Dotfiles variables
export DOTFILES_BIN="$DOTFILES_PATH/bin"

# Default applications
export BROWSER="chrome"
export EDITOR="nvim"
export VISUAL="nvim"
export IDE="code"
export VIDEO_PLAYER="vlc"
export HIGHLIGHTER="highlight"

# Set user-defined locale
export LANG="en_US.UTF-8"

# Turn colors on
export TERM=xterm-256color
export CLICOLOR=1
export LSCOLORS=Gxfxcxdxbxegedabagacad

# Enable colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Set colors for grep
export GREP_COLOR='3;33'

# SSH key path
export SSH_KEY_PATH="~/.ssh/id_ed25519"

# Default less options
export LESS="--hilite-search --IGNORE-CASE --LONG-PROMPT --RAW-CONTROL-CHARS --chop-long-lines --hilite-unread -z-4"

# Better yaourt colors
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

# Pager - use `most`, falling back to `less`
if [ -x "$(command -v most)" ]; then
	export PAGER="most"
else
	export PAGER="less"
fi

# Include $DOTFILES_PATH/bin in PATH if it exists
if [[ -d "$DOTFILES_PATH/bin" ]]; then
	export PATH="$DOTFILES_PATH/bin:$PATH"
fi

# Include $HOME/bin in PATH if it exists
if [[ -d "$HOME/bin" ]]; then
	export PATH="$HOME/bin:$PATH"
fi

# Include $HOME/.local/bin in PATH if it exists
if [ -d "$HOME/.local/bin" ] ; then
	export PATH="$HOME/.local/bin:$PATH"
fi

# Add yarn globals to PATH
if [[ -x "$(command -v yarn)" ]]; then
	# TODO: Pick one?
	export PATH="$(yarn global bin):$PATH"
	export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
fi

# Add go related exports
export GOPATH="$HOME/.go"

# Add global go binaries to PATH
GOBIN="/usr/local/go/bin"
if [[ -d "$GOBIN" ]]; then
	export PATH="$PATH:$GOBIN"
fi
unset GOBIN

# Add user-space go binaries to PATH
GOUBIN="$GOPATH/bin"
if [[ -d "$GOUBIN" ]]; then
	export PATH="$PATH:$GOUBIN"
fi
unset GOUBIN

# Add user-space dart binaries to PATH
DARTUBIN="$HOME/.pub-cache/bin"
if [[ -d "$DARTUBIN" ]]; then
	export PATH="$PATH:$DARTUBIN"
fi
unset DARTUBIN

# Add user-space ruby gem binaries to PATH
GEMUBIN="$HOME/.local/share/gem/ruby/2.7.0/bin"
if [[ -d "$GEMUBIN" ]]; then
	export PATH="$PATH:$GEMUBIN"
fi
unset GEMUBIN

# Export Java home
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk"

# Export Android SDK path
export ANDROID_SDK_ROOT="/opt/android-sdk"
# And add to path
# TODO: Cleanup, test for presence.
export PATH="$PATH:$ANDROID_SDK_ROOT/emulator"
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/"
export PATH="$ANDROID_SDK_ROOT/emulator:$PATH"
