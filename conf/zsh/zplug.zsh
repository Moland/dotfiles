# Define home to Zplug
export ZPLUG_HOME=$HOME/.zplug

# Initiate Zplug
source $ZPLUG_HOME/init.zsh

# Clear packages
zplug clear

# Packages
# ========

# zplug
# -----

zplug "zplug/zplug", hook-build:"zplug --self-manage"

# zsh-users
# ---------

zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-syntax-highlighting", defer:2

# oh-my-zsh - libs
# ----------------

zplug "lib/clipboard", from:oh-my-zsh
zplug "lib/completion", from:oh-my-zsh
# zplug "lib/directories", from:oh-my-zsh
zplug "lib/git", from:oh-my-zsh
# zplug "lib/grep", from:oh-my-zsh
zplug "lib/history", from:oh-my-zsh
zplug "lib/key-bindings", from:oh-my-zsh
zplug "lib/misc", from:oh-my-zsh
zplug "lib/prompt_info_functions", from:oh-my-zsh
zplug "lib/spectrum", from:oh-my-zsh
zplug "lib/termsupport", from:oh-my-zsh
zplug "lib/theme-and-apperance", from:oh-my-zsh

# oh-my-zsh - plugins
# -------------------

# zplug "plugins/adb", from:oh-my-zsh
# zplug "plugins/autoenv", from:oh-my-zsh
# zplug "plugins/battery", from:oh-my-zsh
zplug "plugins/catimg", from:oh-my-zsh
zplug "plugins/colored-man-pages", from:oh-my-zsh
# zplug "plugins/colorize", from:oh-my-zsh
# zplug "plugins/dotenv", from:oh-my-zsh
# zplug "plugins/emoji", from:oh-my-zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/git-flow", from:oh-my-zsh
# zplug "plugins/get-remote-branch", from:oh-my-zsh
# zplug "plugins/gulp", from:oh-my-zsh
# zplug "plugins/vagrant", from:oh-my-zsh
# zplug "plugins/vagrant-prompt", from:oh-my-zsh
# zplug "plugins/yarn", from:oh-my-zsh

# Basics
# ------

zplug "momo-lab/zsh-abbrev-alias"

# Contrib
# -------

# zplug "docker/cli", use:contrib/completion/zsh/_docker
# zplug "docker/compose", use:contrib/completion/zsh/_docker-compose

# SSH
zplug "zpm-zsh/ssh"
# zplug "plugins/ssh-agent", from:oh-my-zsh

# zplug "supercrabtree/k"
# zplug "b4b4r07/enhancd", use:init.sh
# zplug "junegunn/fzf", use:shell/key-bindings.zsh
# zplug "djui/alias-tips"
# zplug "mrowa44/emojify", as:command, use:emojify
# zplug "b4b4r07/emoji-cli"
# zplug "zdharma/zsh-diff-so-fancy", as:command, use:bin/git-dsf

# Themes
# zplug "themes/gallois", from:oh-my-zsh, as:theme
# zplug "b-ryan/powerline-shell" as:theme

# Install new packages
if ! zplug check --verbose; then
	printf "Install? [y/N]: "
	if read -q; then
		echo; zplug install
	else
		# echo
	fi
fi

# Load Zplug
zplug load

# djui/alias-tips
export ZSH_PLUGINS_ALIAS_TIPS_TEXT='💡 Alias tips: '
