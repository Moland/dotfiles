# Directories
# ===========

# Changing directory with just path
setopt AUTO_CD

# Push old directory to stack
setopt AUTO_PUSHD

# ..but don't push dupes
setopt PUSHD_IGNORE_DUPS
