# Correction
# ==========

if [[ "$ENABLE_CORRECTION" == "true" ]]; then
	NOCORRECT_COMMANDS=(cp man mkdir mv mysql sudo)

	for command in ${NOCORRECT_COMMANDS[@]}; do
		alias ${command}="nocorrect ${command}"
	done

	setopt correct_all
fi
