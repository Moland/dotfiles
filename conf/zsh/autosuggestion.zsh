# Configurations for zsh-autosuggestions
# (https://github.com/zsh-users/zsh-autosuggestions)

# Using the matching on latest with preceding history
export ZSH_AUTOSUGGEST_STRATEGY=(match_prev_cmd)

# Bind to accept suggestion on CTRL+Space
bindkey '^ ' autosuggest-accept