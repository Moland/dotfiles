# History
# =======

# History variables
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=$HISTSIZE

# Append to history, writing to HISTFILE
setopt APPEND_HISTORY

# Include more information about when the command was executed, etc
setopt EXTENDED_HISTORY

# Shares history between sessions
setopt SHARE_HISTORY

# Remove extra blanks from each command line being added to history
setopt HIST_REDUCE_BLANKS

# Prioritize trimming dups over unique values in histry.
setopt HIST_EXPIRE_DUPS_FIRST

# Don't record commands with a leading space
setopt HIST_IGNORE_SPACE

# Don't record command if a duplicate exists in history
# setopt HIST_IGNORE_DUPS

# Delete duplicate entries in history if command already exists.
# setopt HIST_IGNORE_ALL_DUPS

# Don't save duplicates to history file
# setopt HIST_SAVE_NO_DUPS

# When searching history don't display results already cycled through twice
setopt HIST_FIND_NO_DUPS
