# Outputs most used commands in history
zsh_history_stats() {
	# Set defaults input values
	declare -i NUM_LINES=10
	declare -i OFFSET=0
	declare -i PAGE=1

	# Flag controlling whether to use OFFSET value as is, or calculate OFFSET from
	# PAGE value multiplied by NUM_LINES
	local OFFSET_FROM_PAGE=false

	# Iterate over argv, switching case between accepted arguments
	while [[ $# -gt 0 ]]; do
		case "$1" in
			# Set NUM_LINES if -n, --num-lines or --lines is provided
			(-n|-n=*)|(--num-lines|--num-lines=*)|(--lines|--lines=*))
				[[ $1 = -n ]] || [[ $1 = --num-lines ]] || [[ $1 = --lines ]] \
				&& NUM_LINES=$2 && shift \
				|| NUM_LINES="${1#*=}"
			;;

			# Set OFFSET if -o, --line-offset or --offest is provided
			(-o|-o=*)|(--line-offset|--line-offset=*)|(--offset|--offset=*))
				[[ $1 = -o ]] || [[ $1 = --line-offset ]] || [[ $1 = --offset ]] \
				&& OFFSET="$2" && shift \
				|| OFFSET="${1#*=}"
				OFFSET_FROM_PAGE=false # Use OFFSET as is
			;;

			# Set OFFSET if -p or --page is provided,
			(-p|-p=*)|(--page|--page=*))
				[[ $1 = -p ]] || [[ $1 = --page ]] \
				&& PAGE="$2" && shift \
				|| PAGE="${1#*=}"
				OFFSET_FROM_PAGE=true # Calculate OFFSET from PAGE
			;;
		esac

		# Shift argument until stack is empty
		shift
	done

	# Set OFFSET to multiplier of PAGE and NUM_LINES if last applied arg is --page
	if [[ $OFFSET_FROM_PAGE = true ]]; then
		OFFSET="$NUM_LINES*($PAGE-1)"
	fi

	# Setup start and end line number as expected by SED
	declare -i START_LINE="$OFFSET+1"
	declare -i END_LINE="$OFFSET+$NUM_LINES"

	awk_command() {
		cat - | awk -v show_details=0 '
			BEGIN {
				if (show_details) {
					print show_details;
					print "yoyoyo";
					exit 0;
				}
			}

			{
				cmd=$2;
				$1="";
				$2="";
				args=$0;
				sub(/^\s+/, "", args);

				data[cmd]["count"]++;
				data[cmd]["args"][args]++;
				total_count++;
			}

			END {
				for (a in data) {
					print data[a]["count"] "|" data[a]["count"]/total_count*100 "%|" a "|" length(data[a]["args"]);

					if (show_details) {
						for (b in data[a]["args"]) {
							print "     - (" data[a]["args"][b] " - " b ")";
						}
					}
				}
			}
		'
	}

	# Run the command pipeline (se below for details)
	fc -l 1 \
	| grep -v "./" \
	| awk_command \
	| sort -nr \
	| nl -w 1 -n "ln" -s "|" \
	| column -t -N "Position,Use count,% of total,Command,Argument variations" -R "Position,Use count,% of total" -c 2 -s "|" -o " | " \
	# | sed -n "${START_LINE},${END_LINE}p; ${END_LINE}q" \
	;

	# The pipeline consists of:
	#
	# 1. fc - list command history
	# 2. grep - remove relative commands
	# 3. awk_command - parse and output relevant data
	# 4. sort - sort list by use count
	# 5. nl - add line number (ie. position)
	# 6. column - format data in table
	# 7. sed - crop result to match number of lines and offset
}
