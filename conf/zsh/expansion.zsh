# Expansion & Globbing
# ====================

# Make '#', '~' and '^' part globbing
setopt EXTENDED_GLOB

# Mark directories with '/' in results
setopt MARK_DIRS

# Sort numeric filenames in numerical order (not lexicographically)
setopt NUMERIC_GLOB_SORT

# Print error if no filenames found
setopt NOMATCH
