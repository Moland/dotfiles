# Compdef aliases
# ---------------
# Enables autocomplete support for alias, using value for search.

compdef g="git"
compdef y="yarn"
compdef la="ls"

# Suffix aliases
# --------------
# Defines a command to use when running file with given suffix as command.

# Open .html files in browser
alias -s html=$BROWSER

# Open code in visual editor
alias -s js=$VISUAL
alias -s ts=$VISUAL
alias -s md=$VISUAL
alias -s json=$VISUAL
alias -s yml=$VISUAL

alias -s mkv=$VIDEO_PLAYER
alias -s mp4=$VIDEO_PLAYER
alias -s avi=$VIDEO_PLAYER

# Abbrivation expansions
# ----------------------
# Defines aliases when typed (followed by space) expands to given value.
#
# Possible alternative to abbrev-alias plugin (see Expanding Global Aliases):
# https://blog.lftechnology.com/command-line-productivity-with-zsh-aliases-28b7cebfdff9

# Initiate abbrev-alias
autoload -z abbrev-alias
abbrev-alias --init

# Simple global aliases
abbrev-alias -g H="| head"
abbrev-alias -g T="| tail"
abbrev-alias -g G="| grep"
abbrev-alias -g E="| ${EDITOR}"
abbrev-alias -g P="| ${PAGER}"
abbrev-alias -g L="| ${PAGER}"
abbrev-alias -g LL="2>&1 | ${PAGER}"
abbrev-alias -g C="| cat"
abbrev-alias -g CA="2>&1 | cat -A"
abbrev-alias -g SORT="| sort"

# Copy / paste
abbrev-alias -g GETC="| clipcopy"
abbrev-alias -g COPY="| clipcopy"
abbrev-alias -g SETC="clippaste |"
abbrev-alias -g PASTE="clippaste |"

# Write stdout to /dev/null
abbrev-alias -g NUL="> /dev/null 2>&1"
abbrev-alias -g NULL="> /dev/null 2>&1"

# Write stderr to /dev/null
abbrev-alias -g NE="2> /dev/null"

# Look here for some inspiration:
# https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/common-aliases/common-aliases.plugin.zsh#L53
