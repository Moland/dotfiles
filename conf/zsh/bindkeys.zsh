# Bind Keys
# =========

# To see the key combo you want to use just do:
# cat > /dev/null
# And press it

bindkey -e

# Go to beginning of line
bindkey '^[[7~' beginning-of-line                               # Home key
bindkey '^[[H' beginning-of-line                                # Home key
if [[ "${terminfo[khome]}" != "" ]]; then
	bindkey "${terminfo[khome]}" beginning-of-line                # [Home] - Go to beginning of line
fi

# Go to end of line
bindkey '^[[8~' end-of-line                                     # End key
bindkey '^[[F' end-of-line                                      # End key
if [[ "${terminfo[kend]}" != "" ]]; then
	bindkey "${terminfo[kend]}" end-of-line                       # [End] - Go to end of line
fi

# Navigate normally
bindkey '^[[C'  forward-char                                    # Right key
bindkey '^[[D'  backward-char                                   # Left key
bindkey '^[[2~' overwrite-mode                                  # Insert key
bindkey '^[[3~' delete-char                                     # Delete key

# Navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word                                     #
bindkey '^[Od' backward-word                                    #
bindkey '^[[1;5D' backward-word                                 # Ctrl+left key
bindkey '^[[1;5C' forward-word                                  # Ctrl+right key

bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '^[[3;5~' vi-forward-blank-word
bindkey '^[[Z' undo                                             # Shift+tab undo last action

# zsh-history-substring-search
bindkey "$terminfo[kcuu1]" history-substring-search-up          # Up key
bindkey "$terminfo[kcud1]" history-substring-search-down        # Down key
bindkey '^[[A' history-substring-search-up                      # Up key
bindkey '^[[B' history-substring-search-down                    # Down key

bindkey '^[[5~' history-beginning-search-backward               # Page up key
bindkey '^[[6~' history-beginning-search-forward                # Page down key
