# Compeletion
# ===========

# Init
# ----

zmodload -i zsh/complist

# automatically load bash completion functions
autoload -U +X bashcompinit && bashcompinit

# Load compinit to get auto completions working
autoload -U compinit && compinit

# Options
# -------

# Show completion menu on successive tab presses
setopt AUTO_MENU

# When completing from the middle of a word, move the cursor to the end of the word
setopt ALWAYS_TO_END

# Automatically list choices on ambiguous completion when called twice
setopt BASH_AUTO_LIST

# Test completion on aliases before substituting
setopt COMPLETE_ALIASES

# Allow completion from within a word/phrase
setopt COMPLETE_IN_WORD

# List completion options when using globs
setopt GLOB_COMPLETE

# Disable beep for completion
setopt NO_LIST_BEEP

# To consider:
# ------------

# zstyle :compinstall filename '/cygdrive/c/Users/tdolsen/.zshrc'

# add in zsh-completions
# fpath=(/opt/boxen/homebrew/share/zsh-completions $fpath)

# should this be in keybindings?
bindkey -M menuselect '^o' accept-and-infer-next-history
zstyle ':completion:*:*:*:*:*' menu select

# man zshcontrib
zstyle ':vcs_info:*' actionformats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:*' enable git #svn cvs

# Complete . and .. special directories
zstyle ':completion:*' special-dirs true

# Enable completion caching, use rehash to clear
zstyle ':completion::complete:*' use-cache on
# TODO: Change to a better cache path variable
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

# Fallback to built in ls colors
# zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)

# Make the list prompt friendly
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'

# Make the selection prompt friendly when there are a lot of choices
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

# Set command for process completion
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm -w -w"

# Add simple colors to kill
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=1 _complete _ignored _approximate

# Insert all expansions for expand completer
# zstyle ':completion:*:expand:*' tag-order all-expansions

# Disable named-directories autocompletion
# zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories

# Match case insensitive, hyphen insensitive, partial word and substring completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'

# Offer indexes before parameters in subscripts
# zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# ! formatting and messages
# zstyle ':completion:*' verbose yes
# zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# Completion order for SCP
zstyle ':completion:*:scp:*' tag-order files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order files all-files users hosts-domain hosts-host hosts-ipaddr

# Completion order for SSH
zstyle ':completion:*:ssh:*' tag-order users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order hosts-domain hosts-host users hosts-ipaddr

# Ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'

# Show ignored items when single item (ie. when we really want to)
zstyle '*' single-ignored show
