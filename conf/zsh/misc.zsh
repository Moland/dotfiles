# Miscellaneous
# =============

# Input/Output
# ------------

# Allow comments even in interactive shells
setopt INTERACTIVE_COMMENTS

# Print non-zero exit values
setopt PRINT_EXIT_VALUE

# Job control
# -----------

# Report background job status immediately
setopt NOTIFY

# Line editor (ZLE)
# -----------------

# No beeping sounds
setopt NO_BEEP

# Mail
# ----

# Print warnings about accessed mail
setopt MAIL_WARNING
