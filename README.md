# .dotfiles

> ~ sweet ~

The files I use to configure and manage my working environment.

### What is configured?

* Basic shell behaviour, exports and aliases
* Bash (minimal setup)
* Zsh
  * zplug dependency manager (with OMZ libs/plugins)
  * Auto suggestion
  * Completions
  * History substring search
  * Syntax highlighting
* Guake
* VS Code

### Also makes backups of:

* Pacman
* Crontabs
* Dconf
