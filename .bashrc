# Source global dotfiles exports
source "${HOME}/.dotfiles_env"

# Source global config
source "${DOTFILES_PATH}/conf/global.sh"

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Shell Options
# =============

# Don't wait for job termination notification
set -o notify

# Don't use ^D to exit
set -o ignoreeof

# cd if valid path even without cd command
shopt -s autocd

# Use case-insensitive filename globbing
shopt -s nocaseglob

# Use "**" for pathname expansion - will match all files and zero or more
# directories and subdirectories.
shopt -s globstar

# Attempt fixing simple spelling errors
shopt -s cdspell

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Expand aliases, also when shell is not interactive.
shopt -s expand_aliases

# History Options
# ===============

# Size limits for terminal history and history file
HISTSIZE=10000
HISTFILESIZE=10000

# Don't put duplicate lines or lines starting with space in the history
HISTCONTROL=ignoreboth

# Make bash append rather than overwrite the history on disk
shopt -s histappend

# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit'
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit:ls' # Ignore the ls command as well

# Whenever displaying the prompt, write the previous line to disk
# export PROMPT_COMMAND="history -a"

# Completion options
# ==================

# These completion tuning parameters change the default behavior of bash_completion:

# Define to access remotely checked-out files over passwordless ssh for CVS
# COMP_CVS_REMOTE=1

# Define to avoid stripping description in --option=description of './configure --help'
# COMP_CONFIGURE_HINTS=1

# Define to avoid flattening internal contents of tar files
# COMP_TAR_INTERNAL_PATHS=1

# Load completions
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		source /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		source /etc/bash_completion
	fi
fi

# Load global aliases
source "${DOTFILES_PATH}/conf/aliases.sh"

# Load additional configs
source "${DOTFILES_PATH}/conf/bash/aliases.sh"
source "${DOTFILES_PATH}/conf/bash/prompt.sh"
