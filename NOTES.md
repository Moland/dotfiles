# Notes

## Install script

1. Make it so VS Code, Pacman, Guake and other config folders can be symlinked

## Immediate tasks

1. Make it easier to configure exports and other dotenv related variables
   * Make `.dotfiles_env` into template and populate during installation with interactive setup
   * Ensure proper default fallbacks
2. Improve installation script
   * Custom package/plugin managment
     * git repo + build instructions
     * arch and yay
   * Fonts
3. [`bash-abbrev-alias`](https://github.com/momo-lab/bash-abbrev-alias) (and share config between zsh and bash)
4. Configure vim environment
5. Configure fuzzy search

## ZSH

**Completions:**

* SSH known hosts

**Plugins:**

* rsync
* screen
* tmux
* mysql-colorize
* safe-paste
