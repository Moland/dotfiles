# Source global dotfiles exports
source "${HOME}/.dotfiles_env"

# Source .bashrc if running bash
if [ -n "${BASH_VERSION}" ]; then
	if [ -f "${HOME}/.bashrc" ]; then
		source "${HOME}/.bashrc"
	fi
fi
