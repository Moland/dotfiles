#!/usr/bin/env bash

# Todo
# ----
# * Backup terminals
# * Handle missing permissions - sudo

# Source dotenv environment variables.
source ~/.dotfiles_env || true

# Ensure needed environment variables are set.
[[ -z $DOTFILES_PATH ]] || [[ -z $BACKUP_PATH ]] || [[ -z $CONFIG_PATH ]] && exit 1

# ! Crontab
# ---------
# Starting by making a copy of the contents of crontab.
crontab -l > ${BACKUP_PATH}/crontab

# ! Pacman
# --------
# We also want to keep all (semi) non-sensitive pacman related.

# Variable referencing where to backup pacman files.
PACMAN_BACKUP_PATH=${BACKUP_PATH}/pacman

# Ensure directory exists.
mkdir -p $PACMAN_BACKUP_PATH

# Output list of all packages and versionsmanaged by pacman.
pacman -Qe > ${PACMAN_BACKUP_PATH}/packages.list

# Copy config and mirrors.
cp -f /etc/pacman.conf ${PACMAN_BACKUP_PATH}
# cp -f /etc/pacman-mirrors.conf ${PACMAN_BACKUP_PATH}
cp -f /etc/pacman.d/mirrorlist ${PACMAN_BACKUP_PATH}

# Copy the whole /etc/pacman.d folder.
# TODO: Fix permission issues here, figure out which files to backup and which
# TODO: ignore. For now we just backup the mirrorlist.
# cp -rf /etc/pacman.d ${PACMAN_BACKUP_PATH}

# ! Dconf dumps
# -------------
# In order to backup configurations of applications using dconf (without
# making a full backup of the whole hierarchy), we pick the namespaces we want
# to dump and store them respectively.

# Variable referencing the Dconf backup paths
DCONF_BACKUP_PATH=${BACKUP_PATH}/dconf

# Define list of application configurations to backup, that are organized
# under the /org/ namespace of dconf.
DCONF_DIRS=(
	/apps/guake
	/org/cinnamon
	/org/gnome
	/org/gnome/terminal
	/org/gtk
	/org/nemo
	/org/virt-manager
	/org/x
)

# Iterate through list and dump config to file
for ORG in "${DCONF_DIRS[@]}"; do
	file=${DCONF_BACKUP_PATH}${ORG}.conf
	mkdir -p "${file%/*}"
	dump=`dconf dump ${ORG}/`
	if [[ -z $dump ]]; then continue; fi
	echo "$dump" > $file
done

# ! VS Code
# ---------
# It being our primary editor, we need to make sure to backup whatever is
# necessary from VS Code. The primary configuration is stored in
# $CONFIG_PATH/vscode/User, and symlinked to the actual installation
# directory. Unfortunately this doesn't give us a good overview of our
# installed extensions, so we take a backup of that ourselves.
code --list-extensions > ${CONFIG_PATH}/vscode/extensions.list

# ! Yarn
# ------
# There's not much interesting to keep about Yarn, but the globally installed
# packages is nice to have as a reference.
cat "$(yarn global dir)/package.json" > ${BACKUP_PATH}/yarn-global-package.json
