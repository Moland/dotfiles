module.exports = {
	// "DARK", "LIGHT" or an object interpreted by IonicaBizau/node-git-stats-colors
	"theme": "DARK",

	// First day of the week
	first_day: "Mon",

	// Show all authors
	authors: false,
};
